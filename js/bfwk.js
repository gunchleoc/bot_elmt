/* bfwk framework */

var last_notification='';
var mobileview=false;
var mobilereturn='';

function modalize_form(selector, checkfunction) {
	
	var url=$(selector).attr('action');
	var modDiv=$(selector).closest('div.modal');
	
	if(modDiv.length===0)
		modDiv=$('#myModalWindow');
	
	if(!checkfunction) {
		checkfunction = function() { return true; };
	}
	$(selector).ajaxForm({
		target:  modDiv,
		beforeSubmit: checkfunction,
		type: 'post',
	});
	
}

function modalize_link(selector) {
	/* modal */
	console.log('modalize_link('+selector+')');
	$(selector).unbind('click');
	$(selector).click(function() {
		var url=$(this).attr('href').substr(1); // remove #
		var selectorTarget=$(this).attr('data-target'); // defined target ?
		var modDiv=null;
		if(selectorTarget.length>0)
			modDiv=$(selectorTarget).first();
		else
			modDiv=$(this).closest('div.modal');
		console.log('open '+url+'...');
		$.ajax({
			url: url,
			success: function(data) {
				$(modDiv).html(data);
			},
			async: false
		});
	});
	
	if(mobileview) {
		$('.close, .btn-close').unbind('click');
		$('.close, .btn-close').click(function(event) {
			event.preventDefault();
			if(mobilereturn!=='')
				document.location.assign(mobilereturn);
			else
				window.history.back();
		});
	}
}

	/* modal */
function active_modal_links(selector)
{
	var selectr = (typeof selector !== 'undefined') ? (selector+' ') : '';
	$(selectr+'a.ajax-modal').unbind('click');
	$(selectr+'a.ajax-modal').click(function() {
		var url=$(this).attr('href').substr(1); // remove #
		var modDiv=$(this).attr('data-target');
		
		$.ajax({
			url: url,
			success: function(data) {
				$(modDiv).html(data);
				modalize_link(modDiv + ' a.ajax-modal');
			},
			async: false
		});
	});
}

$().ready(function() {
	
	/* modal */
	active_modal_links();
	var datatable_options_ok = {
		"language": { "url": "/js/dataTables.fr.json" },
		"order": [], 
	};
	if(typeof(datatable_options) !== "undefined") {
		datatable_options_ok = datatable_options;
	}
	$('table.table-datatable').DataTable(datatable_options_ok);

	if(!onload) {
		onload = function() { };
	}
	onload();

});
