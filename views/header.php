<!doctype html>
<html lang="<?=BFWK_LANG?>">
<head>
	<meta charset="utf-8">
	<?php if(isset($refresh_url)) { ?>
	<meta http-equiv="refresh" content="0; url=<?=$refresh_url?>">
	<?php } ?>
	<title><?=isset($title)&&$title!=''?$title:'no_title_set'//The page title?> - <?=t('ApplicationName')?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link type="text/css" href="css/bootstrap.min.css" rel="stylesheet" />
	<link type="text/css" href="css/fork-awesome.min.css" rel="stylesheet" />
	<link type="text/css" href="css/sticky-footer.css" rel="stylesheet" /> 
	<link type="text/css" href="css/dataTables.bootstrap4.min.css" rel="stylesheet" />
	<link type="text/css" href="css/custom.css" rel="stylesheet" />	

	<link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body class="ctrl_<?=$_ctrl?> view_<?=$_viewpage?>" id="body">

<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
	<div class="container">
		<a href="?/" class="navbar-brand"><img src="/img/favicon/favicon-32x32.png"/> <?=t('ApplicationName')?> </a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="nav navbar-nav">
				<li class="nav-item">
					<a class="nav-link ajax-modal" href="#?/main/modal_city" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-map-marker"></i> <?=t('Choose_your_location')?></a>
				</li>
			</ul>
			<?php if( isset($GLOBALS['langs']) && count($GLOBALS['langs'])>1 ) { ?>
			<ul class="nav navbar-nav ml-auto">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-fw fa-lg fa-globe"></i> Language<span class="caret"></span></a>
					<div class="dropdown-menu">
			<?php	foreach($GLOBALS['langs'] as $lang ) { ?>
						<a id="lang-<?=$lang?>" class="dropdown-item ajax-modal" href="#?/main/modal_lang/<?=$lang?>&userlang=<?=$lang?>" data-toggle="modal" data-target="#myModalWindow"><i class="fa fa-fw fa-lg fa-book"></i> <?=$lang?></a>
			<?php 	} ?>
					</div>
				</li>
			</ul>
			<?php } ?>
		</div>
	</div>
</nav>
<div style="height:58px;"> </div>
<!-- /Navbar -->

<!-- Modal container -->
<div class="modal fade" id="myModalWindow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<p>modal window</p>
</div>
<!-- end modal -->

<div class="container">
