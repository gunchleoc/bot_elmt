<?php

class txt
{
	
	static public function base64_url_encode($input) {
		return strtr(base64_encode($input), '+/=', '._-');
	}
	
	static public function base64_url_decode($input) {
		return base64_decode(strtr($input, '._-', '+/='));
	}
	
	static public function url($txt)
	{
		$low = trim(strtolower(iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $txt)));
		$ln = strlen($low);
		$url = '';
		$spec=true;
		for($i=0;$i<$ln;$i++)
		{
			$c=$low[$i];
			if($c>='0' && $c<='9' || $c>='a' && $c<='z')
			{
				$url.=$c;
				$spec=false;
			}
			else
			{
				if(!$spec)
					$url.='-';
				$spec=true;
			}
		}
		return $url;
	}
	
	
	static public function correctTel($tel, $pays)
	{
		$telModifie = trim($tel);
		$telModifie = str_replace(' ', '', $telModifie);
		$telModifie = preg_replace('#[^0-9]+#u', '', $telModifie);
		if($pays == 'France')
		{
			if(strlen($telModifie) === 10) // 0101010101
			{
				if(substr($telModifie,0,1)!='0')
					return false;
				return '0033'.substr($telModifie,1);
			}
			if(strlen($telModifie) === 11)	// 33101010101
			{
				if(substr($telModifie,0,2)!='33')
					return false;
				return '00'.$telModifie;
			}
			if(strlen($telModifie) === 13)	// 0033101010101
			{
				if(substr($telModifie,0,4)!='0033')
					return false;
				return $telModifie;
			}
		}
		
		if($pays == 'Suisse')
		{
			if(strlen($telModifie) === 9) // 0101010101
			{
				if(substr($telModifie,0,1)!='0')
					return false;
				return '0041'.substr($telModifie,1);
			}
			if(strlen($telModifie) === 10) // 0101010101
			{
				if(substr($telModifie,0,1)!='0')
					return false;
				return '0041'.substr($telModifie,1);
			}
			if(strlen($telModifie) === 11)	// 41101010101
			{
				if(substr($telModifie,0,2)!='41')
					return false;
				return '00'.$telModifie;
			}
			if(strlen($telModifie) === 13)	// 0041101010101
			{
				if(substr($telModifie,0,4)!='0041')
					return false;
				return $telModifie;
			}
		}
		
		if($pays == 'Belgique')
		{
			if(strlen($telModifie) === 8) // numéro pour les petites villes/provinces
			{
				if(substr($telModifie,0,1)!='0')
					return false;
				return '0032'.substr($telModifie,1);
			}
			if(strlen($telModifie) === 9) // numéro pour les grandes villes/provinces
			{
				if(substr($telModifie,0,1)!='0')
					return false;
				return '0032'.substr($telModifie,1);
			}
			if(strlen($telModifie) === 10) // numéro de portable
			{
				if(substr($telModifie,0,1)!='0')
					return false;
				return '0032'.substr($telModifie,1);
			}
			/*if(strlen($telModifie) === 10)	// 3210101010
			{
				if(substr($telModifie,0,2)!='32')
					return false;
				return '00'.$telModifie;
			}
			if(strlen($telModifie) === 12)	// 003210101010
			{
				if(substr($telModifie,0,4)!='0032')
					return false;
				return $telModifie;
			}*/
			if(strlen($telModifie) === 11)
			{
				if(substr($telModifie,0,4)!='0032')
					return false;
				return $telModifie;
			}
			if(strlen($telModifie) === 12)
			{
				if(substr($telModifie,0,4)!='0032')
					return false;
				return $telModifie;
			}
			if(strlen($telModifie) === 13)
			{
				if(substr($telModifie,0,4)!='0032')
					return false;
				return $telModifie;
			}
		}
		
		return false;
	}
	
	static public function simpleTel($tel)
	{
		if(substr($tel,0,2)=='00')
			return '0'.substr($tel,4);
		else
			return $tel;
	}


	static public function fill_template_file($strFileTemplate, $arrValues = array()) {
		// Vérification que le fichier template existe
		if (!file_exists($strFileTemplate))
		// pas de fichier valide
			return "";

		// récupération du contenu du fichier
		$strContent = file_get_contents($strFileTemplate);
		if (trim($strContent) == "")
		// fichier vide, retour immédiat
			return "";

		// remplacement des valeurs clés
		if (count($arrValues) > 0)
			$strContent = str_replace(array_keys($arrValues), array_values($arrValues), $strContent);

		// retour de la fonction
		return $strContent;
	}
	
	static public function unique_id($numStr) { //on génère une chaine unique et automatique
		srand((double)microtime()*rand(1000000,9999999) ); // Genere un nombre aléatoire
		$arrChar = array(); // Nouveau tableau
		for( $i=65; $i<90; $i++ ) {
			array_push( $arrChar, chr($i) ); // Ajoute A-Z au tableau
			array_push( $arrChar, strtolower( chr( $i ) ) ); // Ajouter a-z au tableau
		}
		for( $i=48; $i<57; $i++ ) {
			array_push( $arrChar, chr( $i ) ); // Ajoute 0-9 au tableau
		}
		for( $i=0; $i< $numStr; $i++ ) {
			$uId .= $arrChar[rand( 0, count( $arrChar ) )]; // Ecrit un aléatoire
		}
		return $uId;
		//pour le générer : unique_id(5) (nombre de caractères souhaité)
	}
	
	/**
	 * convert YYYY-MM-DD HH:MM:SS to DD/MM/YYYY HH:MM:SS
	 */
	static public function local_datetime($sql_date)
	{
		return date('d/m/Y H:i:s', strtotime($sql_date));
	}
	
	/**
	 * convert number of seconds to XhXXmXXs string
	 */
	static public function seconds_to_time($secs)
	{
		$tim='';
		$secs = intval($secs);
		$h = floor($secs/3600);
		if($h>0) {
			$tim.=$h.'h';
			$secs-=$h*3600;
		}
		$m = floor($secs/60);
		if($m>0) {
			$tim.=sprintf('%02d',$m).'m';
			$secs-=$m*60;
		}
		$tim.=sprintf('%02d',$secs).'s';
		return $tim;
	}
	
	/**
	 * Verification du format d'une adresse email
	 *
	 * @param string $email
	 * @return boolean
	 */
	static public function checkEmail($email)
	{
		if( strpos($email,' ') === false && (
			(preg_match('/(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/', $email)) ||
    		(preg_match('/^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/',$email))) )
		{
			return true;
		}
		return false;
	}
	
	/**
	 * Generate token
	 *
	 * @return token
	 */
	static public function getToken()
	{
		//Generate a random string.
		$token = openssl_random_pseudo_bytes(16);
		 
		//Convert the binary data into hexadecimal representation.
		$token = bin2hex($token);
		 
		//Print it out for example purposes.
		return $token;

	}
}
