<?php

class upload
{
	private static $types = array(
		'gif' => array('image/gif'),
		'jpg' => array('image/jpeg'),
		'pdf' => array('application/pdf', 'application/vnd.cups-pdf'),
		'png' => array('image/png'),
	);
	
	/**
	 * Check a file from a FORM before uploading
	 * @param $postname = <input name>
	 * @param $accepted_types = array('jpg', 'png') or false for all
	 */
	static public function check($postname, $accepted_types=false)
	{
		if(is_uploaded_file($_FILES[$postname]['tmp_name']))
		{
			if($accepted_types!==false)
			{
				$mime = mime_content_type($_FILES[$postname]["tmp_name"]);
				foreach($accepted_types as $accepted_type)
				{
					if(isset(self::$types[$accepted_type]) && in_array($mime, self::$types[$accepted_type]))
						return "OK";
				}
				return "Votre fichier ".$_FILES[$postname]['name']." ne correspond pas au type de fichier attendu : ".implode(', ',$accepted_types);
			}
			return 'OK';
		}
		
		switch($_FILES[$postname]['error'])
		{
		case 0: //no error; possible file attack!
			return "Il y a eu un problème lors du transfert de votre fichier ".$_FILES[$postname]['name'];
			break;
		case 1: //uploaded file exceeds the upload_max_filesize directive in php.ini
			return "Votre fichier ".$_FILES[$postname]['name']." dépasse la taille autorisée par le serveur de ".number_format(ini_get('upload_max_filesize')/(1024*1024),2,',',' ')." Mo";
			break;
		case 2: //uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form
			return "Votre fichier ".$_FILES[$postname]['name']." dépasse la taille autorisée de ".number_format($_POST['MAX_FILE_SIZE']/(1024*1024),2,',',' ')." Mo";
			break;
		case 3: //uploaded file was only partially uploaded
			return "Votre fichier ".$_FILES[$postname]['name']." est arrivé partiellement.";
			break;
		case 4: //no file was uploaded
			return "Votre fichier ".$_FILES[$postname]['name']." est introuvable.";
			break;
		default: //a default error, just in case!  :)
			return "Erreur lors du transfert de votre fichier ".$_FILES[$postname]['name'];
			break;
		}
		
	}
	
	/**
	 * Upload a file from a FORM
	 * @param $postname = <input name>
	 * @param $to_path = path to store file (with  trailing /)
	 * @param $accepted_types = array('JPG', 'PNG') or false for all
	 */
	static public function move($postname, $to_path)
	{
		@mkdir(dirname($to_path), 0777, true);
		
		if (move_uploaded_file($_FILES[$postname]["tmp_name"], $to_path))
		{
			return "OK";
		}
		else
		{
			$err = error_get_last();
			return "Erreur lors de la récupération de votre fichier ".$_FILES[$postname]['name']." : ".$err['message'];
		}
	}
	
	static public function clean_name($postname)
	{
		$name = trim(strtolower(iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $_FILES[$postname]['name'])));
		$dotpos = strrpos($name,'.');
		$base = substr($name,0,$dotpos);
		$ext = substr($name,$dotpos+1);
		return txt::url($base).'.'.txt::url($ext);
	}
	
	
}
